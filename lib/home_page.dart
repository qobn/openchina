import 'package:flutter/material.dart';
import 'package:openchina/constants/constants.dart' show AppColors;
import 'package:openchina/pages/discovery_page.dart';
import 'package:openchina/pages/new_list_page.dart';
import 'package:openchina/pages/news_list_page.dart';
import 'package:openchina/pages/profile_page.dart';
import 'package:openchina/pages/publist_tweet_page.dart';
import 'package:openchina/pages/tweet_page.dart';
import 'package:openchina/widgets/my_drawer.dart';
import 'package:openchina/widgets/navigation_icon_view.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _appBarTitle = ['咨讯', '动弹', '发现', '我的'];
  List<NavigationIconView> _navigationiconviews;
  var _currentIndex = 0;
  List<Widget> _pages;
  PageController _pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _navigationiconviews = [
      NavigationIconView(
          title: "咨讯",
          iconPath: 'assets/images/ic_nav_news_normal.png',
          activedIconPath: 'assets/images/ic_nav_news_actived.png'),
      NavigationIconView(
          title: "动弹",
          iconPath: 'assets/images/ic_nav_tweet_normal.png',
          activedIconPath: 'assets/images/ic_nav_tweet_actived.png'),
      NavigationIconView(
          title: "发现",
          iconPath: 'assets/images/ic_nav_discover_normal.png',
          activedIconPath: 'assets/images/ic_nav_discover_actived.png'),
      NavigationIconView(
          title: "我的",
          iconPath: 'assets/images/ic_nav_my_normal.png',
          activedIconPath: 'assets/images/ic_nav_my_pressed.png'),
    ];
    _pages = [
      NewListPage(),//这个原生控件 上拉加载要用scroll控制器实现 --- 1这个可以互相切换
//      NewsListPage(),//这个用插件实现的上拉加载下拉刷新 --- 2 这个可以互相切换
      TweetPage(),
      DiscoveryPage(),
      ProfilePage(),
    ];
    _pageController = PageController(initialPage: _currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    //SafeArea适配异形屏 刘海屏
    if(_currentIndex==1){
      return Scaffold(
        //TODO 标题 动态改变
        appBar: AppBar(
          title: Text(
            _appBarTitle[_currentIndex],
            style: TextStyle(
              color: Color(AppColors.APPBAR),
            ),
          ),
          iconTheme: IconThemeData(
            color: Color(AppColors.APPBAR),
          ),
          elevation: 0.0,
          actions: <Widget>[
            FlatButton(onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>PublishTweetPage()),);
            }, child: Text('发布'),),
          ],
        ),
        body: PageView.builder(
          physics: NeverScrollableScrollPhysics(),//设置物理线上也就是是否能滑动
          itemBuilder: (BuildContext context, int index) {
            return _pages[index];
          },
          controller: _pageController,
          itemCount: _pages.length,
          onPageChanged: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),

        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          items: _navigationiconviews.map((view) => view.item).toList(),
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
            _pageController.animateToPage(index,
                duration: Duration(microseconds: 1), curve: Curves.ease);
          },
        ),
        drawer: MyDrawer(
          headImgPath: "assets/images/cover_img.jpg",
          menuTitle: ['发布动弹', '动弹小黑屋', '关于', '设置'],
          menuIcons: [Icons.send, Icons.block, Icons.error, Icons.settings],
        ),
      );
    }else{
      return Scaffold(
        //TODO 标题 动态改变
        appBar: AppBar(
          title: Text(
            _appBarTitle[_currentIndex],
            style: TextStyle(
              color: Color(AppColors.APPBAR),
            ),
          ),
          iconTheme: IconThemeData(
            color: Color(AppColors.APPBAR),
          ),
          elevation: 0.0,
        ),
        body: PageView.builder(
          physics: NeverScrollableScrollPhysics(),//设置物理线上也就是是否能滑动
          itemBuilder: (BuildContext context, int index) {
            return _pages[index];
          },
          controller: _pageController,
          itemCount: _pages.length,
          onPageChanged: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),

        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          items: _navigationiconviews.map((view) => view.item).toList(),
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
            _pageController.animateToPage(index,
                duration: Duration(microseconds: 1), curve: Curves.ease);
          },
        ),
        drawer: MyDrawer(
          headImgPath: "assets/images/cover_img.jpg",
          menuTitle: ['发布动弹', '动弹小黑屋', '关于', '设置'],
          menuIcons: [Icons.send, Icons.block, Icons.error, Icons.settings],
        ),
      );
    }

  }
}
