import 'dart:io';

import 'package:flutter/material.dart';
import 'package:openchina/constants/constants.dart';
import 'package:toast/toast.dart';
import 'package:image_picker/image_picker.dart';

class PublishTweetPage extends StatefulWidget {
  @override
  _PublishTweetPageState createState() => _PublishTweetPageState();
}

class _PublishTweetPageState extends State<PublishTweetPage> {
  TextEditingController _textEditingController = TextEditingController();
  List<File> fileList = [];
  Future<File> _imageFile = null;
  bool isLoading = false;
  var _scaffoldkey = new GlobalKey<ScaffoldState>();

  _pickImage(BuildContext context) {
    if (fileList.length >= 9) {
      var snackBar = SnackBar(content: Text("最多只能添加9张图片！"));
      _scaffoldkey.currentState.showSnackBar(snackBar);
      return;
    }
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 130.0,
          child: Column(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  if (mounted) {
                    setState(() {
                      _imageFile = ImagePicker.pickImage(source: ImageSource.camera,maxWidth:70.0,maxHeight:70.0,imageQuality: 100);
                    });
                  }
                },
                child: Container(
                  height: 60.0,
                  child: Center(
                    child: Text(
                      '相机拍照',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                ),
              ),
              new Divider(
                height: 1.0,
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  if (mounted) {
                    setState(() {
                      _imageFile = ImagePicker.pickImage(source: ImageSource.gallery,maxWidth:70.0,maxHeight:70.0,imageQuality: 100);
                    });
                  }
                },
                child: Container(
                  height: 60.0,
                  child: Center(
                    child: Text(
                      '图库选择照片',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        elevation: 0.0,
        title: Text('发布动弹',style: TextStyle(color: Color(AppColors.APPBAR),),),
        iconTheme: IconThemeData(color: Color(AppColors.APPBAR),),
        actions: <Widget>[
          FlatButton(onPressed: (){
            Toast.show('ss', context,duration: Toast.LENGTH_LONG);
          },child: Text('发布'),),
        ],
      ),
      body: FutureBuilder(
          future: _imageFile,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.data != null &&
                _imageFile != null) {
              fileList.add(snapshot.data);
              _imageFile = null;
            }
            return _bodyWidget();
          }),
    );
  }

  Widget _bodyWidget() {
    if (fileList.length >= 9) {
      return ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextField(
              controller: _textEditingController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                hintText: '今天想动弹啥？',
                hintStyle: TextStyle(
                  color: Color(0x25874136),
                ),
              ),
              maxLines: 6,
              maxLength: 150,
            ),
          ),
          GridView.count(
            shrinkWrap: true,
            crossAxisCount: 4,
            children: List.generate(fileList.length, (index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.file(
                  fileList[index],
                  fit: BoxFit.fill,
                ),
              );
            }),
          ),
        ],
      );
    } else {
      return ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextField(
              controller: _textEditingController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                hintText: '今天想动弹啥？',
                hintStyle: TextStyle(
                  color: Color(0x25874136),
                ),
              ),
              maxLines: 6,
              maxLength: 150,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: 4,
              children: List.generate(fileList.length + 1, (index) {
                if (index == fileList.length) {
                  return GestureDetector(
                    onTap: () {
                      _pickImage(context);
                    },
                    child: Image.asset('assets/images/add_icon.png'),
                  );
                } else {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.file(
                      fileList[index],
                      fit: BoxFit.fill,
                    ),
                  );
                }
              }),
            ),
          ),
        ],
      );
    }
  }
}
