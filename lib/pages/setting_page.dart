import 'package:flutter/material.dart';
import 'package:openchina/common/event_bus.dart';
import 'package:openchina/constants/constants.dart' show AppColors;
import 'package:openchina/utils/data_util.dart';

class SettingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          '设置',
          style: TextStyle(color: Color(AppColors.APPBAR),),
          
        ),
        iconTheme: IconThemeData(color: Color(AppColors.APPBAR),),
      ),
      body: Center(child: FlatButton(onPressed: (){
        //退出登录
        DataUtil.clearLoginInfo().then((_){
          eventBus.fire(LogoutEvent());
          Navigator.of(context).pop();
        });
      }, child: Text('退出登录',style: TextStyle(fontSize: 20.0),),),),
    );
  }
}
