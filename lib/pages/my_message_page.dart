import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:openchina/constants/constants.dart' show AppColors, AppUrls;
import 'package:openchina/utils/data_util.dart';
import 'package:openchina/utils/net_util.dart';
//import 'package:pull_to_refresh/pull_to_refresh.dart';

class MyMessagePage extends StatefulWidget {
  @override
  _MyMessagePageState createState() => _MyMessagePageState();
}

class _MyMessagePageState extends State<MyMessagePage> {
  List<String> _tabTitles = ['@我', '评论', '私信'];
  int curPage = 1;
  List messageList;
//  ScrollController _controller = ScrollController();
  bool isMore = false;
//  RefreshController _refreshController = RefreshController(initialRefresh: false);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    _controller.addListener((){
//      if (_controller.position.pixels ==
//          _controller.position.maxScrollExtent) {
//        print('滑动到了最底部');
//        curPage+=1;
//        setState(() {
//          isMore = true;
//        });
//        _getMessageList();
//      }
//    });
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _tabTitles.length,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            '消息中心',
            style: TextStyle(
              color: Color(AppColors.APPBAR),
            ),
          ),
          iconTheme: IconThemeData(
            color: Color(AppColors.APPBAR),
          ),
//          bottom: TabBar(tabs: _tabTitles.map((title)=>Tab(text: title,),).toList(),),//胖箭头可以用
          bottom: TabBar(
              labelColor: Color(AppColors.APPBAR),
              tabs: _tabTitles.map((title) {
                //这种判断也可以用//tab 最后是个List<Widget> 所以最后map要toList转化一下
                return Tab(
                  text: title,
                );
              }).toList()),
        ),
        body: TabBarView(children: [
          Center(
            child: Text('暂无内容'),
          ),
          Center(
            child: Text('暂无内容'),
          ),
          _buildMessageList(),
        ]),
      ),
    );
  }

  _buildMessageList() {
    if (messageList == null) {
      _getMessageList();
      return Center(
        child: CupertinoActivityIndicator(),
      );
    }
    return RefreshIndicator(
        child: ListView.separated(
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    Image.network(messageList[index]['portrait']),
                    SizedBox(
                      width: 10.0,
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                '${messageList[index]['sendername']}',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '${messageList[index]['pubDate']}',
                                style: TextStyle(
                                    fontSize: 12.0, color: Color(0xffaaaaaa)),
                              ),
                            ],
                          ),
                          Text(
                            '${messageList[index]['content']}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(fontSize: 12.0),
                          ),
                        ],
                      ),
                    ),
//                    new Offstage(
//                      offstage: true, //这里控制
//                      child: Container(color: Colors.blue,height: 100.0,child: Text('加载更多'),),
//                    ),
                  ],
                ),
              );
            },
            separatorBuilder: (context, index) {
              if (index == messageList.length) {
                return Container(height: 50.0, child: Text('加载更多'),);
              } else {
                return Divider();
              }
            },
            itemCount: messageList.length),
        onRefresh: _pullToRefresh);

  }

  Future<Null> _pullToRefresh() async {
    curPage = 1;
    _getMessageList();
    return null;
  }

  void _getMessageList() {
    DataUtil.isLogin().then((isLogin) {
      if (isLogin) {
        DataUtil.getTokenAccess().then((accessToken) {
          //拼装请求
          Map<String, dynamic> params = Map<String, dynamic>();
          params['dataType'] = 'json';
          params['page'] = curPage;
          params['pageSize'] = 10;
          params['access_token'] = accessToken;
          NetUtil.get(AppUrls.MESSAGE_LIST, params).then((data) {
            print('MESSAGE_LIST: $data');
            Map<String, dynamic> map = json.decode(data);
            var _messageList = map['messageList'];
            setState(() {
              messageList = _messageList;
              isMore = false;
            });
          });
        });
      }
    });
  }
}
