import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:openchina/pages/common_web_page.dart';
import 'package:openchina/pages/shake_page.dart';
import 'package:sensors/sensors.dart';
import 'package:toast/toast.dart';

class DiscoveryPage extends StatefulWidget {
  @override
  _DiscoveryPageState createState() => _DiscoveryPageState();
}

class _DiscoveryPageState extends State<DiscoveryPage> {
  List<Map<String, IconData>> blocks = [
    {
      '开源众包': Icons.open_in_browser,
      '开源软件': Icons.surround_sound,
      '码云推荐': Icons.crop_original,
      '代码片段': Icons.high_quality,
    },
    {
      '扫一扫': Icons.scanner,
      '摇一摇': Icons.scatter_plot,
    },
    {
      '码云封面人物': Icons.people,
      '线下活动': Icons.local_activity,
    }
  ];

  Future _scan() async {
    String barcode = await BarcodeScanner.scan();
    print('扫码 $barcode');
    Toast.show('扫码 $barcode', context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
//    setState(() {
//      return barcode;
//    });
  }

  void _handleItemClick(BuildContext context, String title) {
    switch (title) {
      case '开源众包':
        _navToWebPage(context, title, 'https://zb.oschina.net/');
        break;
      case '开源软件':
        _navToWebPage(context, title, 'https://www.oschina.net/project');
        break;
      case '码云推荐':
        _navToWebPage(context, title, 'https://gitee.com/explore');
        break;
      case '代码片段':
        _navToWebPage(context, title, 'https://gitee.com/explore');
        break;
      case '码云封面人物':
        _navToWebPage(context, title, 'https://gitee.com/gitee-stars');
        break;
      case '扫一扫':
        _scan();
        break;
      case '摇一摇':
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) {
              return ShakePage();
            },
          ),
        );
        break;
    }
  }

  void _navToWebPage(BuildContext context, String title, String url) {
    if (title != null && url != null) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) {
            return CommonWebPage(title: title, url: url);
          },
        ),
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: blocks.length,
        itemBuilder: (context, blockIndex) {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 10.0),
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1.0,
                  color: Color(0xffaaaaaa),
                ),
                bottom: BorderSide(
                  width: 1.0,
                  color: Color(0xffaaaaaa),
                ),
              ),
            ),
            child: ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                //禁止滑动
                shrinkWrap: true,
                //设置为true可以显示完全
                itemBuilder: (context, mapIndex) {
                  return InkWell(
                    onTap: () {
                      _handleItemClick(
                          context, blocks[blockIndex].keys.elementAt(mapIndex));
                    },
                    child: Container(
                      height: 50.0,
                      child: ListTile(
                        leading: Icon(
                          blocks[blockIndex].values.elementAt(mapIndex),
                        ),
                        title: Text(
                          blocks[blockIndex].keys.elementAt(mapIndex),
                        ),
                        trailing: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, mapIndex) {
                  return Divider(
                    height: 1.0,
                    color: Color(0x47856928),
                  );
                },
                itemCount: blocks[blockIndex].length),
          );
        });
  }
}
