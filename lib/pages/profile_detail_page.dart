import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:openchina/constants/constants.dart'
    show AppColors, AppInfo, AppUrls;
import 'package:openchina/models/user_info.dart';
import 'package:openchina/utils/data_util.dart';
import 'package:openchina/utils/net_util.dart';

class ProFileDetailPage extends StatefulWidget {
  @override
  _ProFileDetailPageState createState() => _ProFileDetailPageState();
}

class _ProFileDetailPageState extends State<ProFileDetailPage> {
  UserInfo _userInfo;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getDetailInfo();
  }

  void _getDetailInfo() {
    DataUtil.getTokenAccess().then((accessToken){
      Map<String, dynamic> params = new Map();
      params['access_token'] = accessToken;
      params['dataType	'] = AppInfo.JSON;
      NetUtil.get(AppUrls.MY_INFORMATION, params).then((data) {
        print('个人信息 $data');
        Map<String, dynamic> map = json.decode(data);
        if (mounted) {
          UserInfo userInfo = new UserInfo();
          userInfo.uid = map['uid'];
          userInfo.name = map['name'];
          userInfo.gender = map['gender'];
          userInfo.province = map['province'];
          userInfo.city = map['city'];
          userInfo.platforms = map['platforms'];
          userInfo.expertise = map['expertise'];
          userInfo.joinTime = map['joinTime'];
          userInfo.lastLoginTime = map['lastLoginTime'];
          userInfo.portrait = map['portrait'];
          userInfo.portrait = map['portrait'];
          userInfo.fansCount = map['fansCount'];
          userInfo.favoriteCount = map['favoriteCount'];
          userInfo.favoriteCount = map['favoriteCount'];
          userInfo.followersCount = map['followersCount'];
          userInfo.notice = map['notice'];
          setState(() {
            _userInfo = userInfo;
          });
        }
      });
    });


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          '用户详情',
          style: TextStyle(
            color: Color(AppColors.APPBAR),
          ),
        ),
        iconTheme: IconThemeData(
          color: Color(AppColors.APPBAR),
        ),
      ),
      body: _buildSingleChildScrollView(),
    );
  }

  SingleChildScrollView _buildSingleChildScrollView() {
    return SingleChildScrollView(
      child: _userInfo == null
          ? Center(
              child: CupertinoActivityIndicator(),
            )
          : Column(
              children: <Widget>[
                //InkWell墨水事件点击效果
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("头像"),
                        Container(
                          width: 60.0,
                          height: 60.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: 2.0,
                              color: Color(AppColors.APPBAR),
                            ),
                            image: DecorationImage(
                              image: NetworkImage(_userInfo.portrait),
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("昵称"),
                        Text(_userInfo.name),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("加入时间"),
                        Text(_userInfo.joinTime),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("所在地区"),
                        Text(_userInfo.province),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("开发平台"),
                        Text(_userInfo.platforms.toString().replaceAll('[', '').replaceAll(']','')),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("专长领域"),
                        Text(_userInfo.expertise.toString().replaceAll('[', '').replaceAll(']','')),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("粉丝"),
                        Text(_userInfo.fansCount.toString()),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("收藏"),
                        Text(_userInfo.favoriteCount.toString()),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0,top: 0.0,right: 10.0,bottom: 10.0),
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("关注"),
                        Text(_userInfo.followersCount.toString()),
                      ],
                    ),
                  ),
                ),
                Divider(height: 1.0,color: Color(AppColors.DIVIDER_LINE),),
              ],
            ),
    );
  }
}
