import 'package:flutter/material.dart';
import 'package:openchina/constants/constants.dart';
import 'package:openchina/models/new_list.dart';
import 'package:openchina/pages/news_detail_page.dart';

class NewListItemPull extends StatelessWidget {
  final NewList newList;

  NewListItemPull({this.newList}) : assert(newList != null);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => NewsDetailPage(
              id: newList.id,
            ),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Color(0x47852369),
              width: 1.0,
            ),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              height: 5.0,
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Text(
                '@${newList.author.replaceAll(" ", "")}_${newList.title}',
                style: TextStyle(
                  fontSize: 15.0,
                ),
//                maxLines: 2,
//                overflow: TextOverflow.ellipsis,
              ),
            ),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  color: Color(AppColors.APPBAR),
                  margin: EdgeInsets.only(left: 10.0),
                  child: Text('${newList.pubDate}'),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.message),
                    Container(
                        margin: EdgeInsets.only(right: 10.0),
                        child: Text('${newList.commentCount}')),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
