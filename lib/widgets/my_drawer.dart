import 'package:flutter/material.dart';
import 'package:openchina/pages/about_page.dart';
import 'package:openchina/pages/publist_tweet_page.dart';
import 'package:openchina/pages/setting_page.dart';
import 'package:openchina/pages/tweet_block_page.dart';

class MyDrawer extends StatelessWidget {
  final String headImgPath;
  final List menuTitle;
  final List menuIcons;

  MyDrawer(
      {Key key,
      @required this.headImgPath,
      @required this.menuTitle,
      @required this.menuIcons})
      : assert(headImgPath != null),
        assert(menuTitle != null),
        assert(menuIcons != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 0.0,
      child: ListView.separated(
        padding: EdgeInsets.all(0.0),//解决drawer顶部阴影
        itemCount: menuTitle.length+1,
        itemBuilder: (context,index){
        if(index==0){
          return Image.asset(headImgPath,fit: BoxFit.cover,);
        }
        index-=1;
        return ListTile(
          leading: Icon(menuIcons[index]),
          title: Text(menuTitle[index]),
          trailing: Icon(Icons.arrow_forward_ios),
          onTap: () {
            //TODO 点击实现
            switch (index) {
              case 0: //PublishTweetPage
                _navPush(context, PublishTweetPage());
                break;
              case 1: //TweetBlockPage
                _navPush(context, TweetBlockPage());
                break;
              case 2: //AboutPage
                _navPush(context, AboutPage());
                break;
              case 3: //SettingPage
                _navPush(context, SettingPage());
                break;
            }
          },
        );
      }, separatorBuilder: (context, index) {
        if (index == 0) {
          return Divider(height: 0.0,);
        } else{
          return Divider(height: 1.0,);
        }
      },
      ),
    );
  }
  _navPush(BuildContext context,Widget page){
//    return Navigator.push(context, MaterialPageRoute(builder: (context){
//      return page;
//    }));
    return Navigator.push(context, MaterialPageRoute(builder: (context)=>page));
  }
}
