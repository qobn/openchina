import 'package:flutter/material.dart';
import 'package:openchina/models/tweet_new.dart';
import 'package:openchina/pages/publist_tweet_page.dart';

class TweetNewItem extends StatelessWidget {
  final TweetNew tweetNew;

  TweetNewItem({this.tweetNew}) : assert(tweetNew != null);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context){
          return PublishTweetPage();
        }));
      },
      child: Stack(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              tweetNew.imgBig == null
                  ? Container(
                margin: EdgeInsets.only(left: 10.0,top: 10.0),
                width: 50.0,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0x47852369),
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image:
                    AssetImage('assets/images/ic_avatar_default.png'),
                  ),
                ),
              )
                  : Container(
                margin: EdgeInsets.only(left: 10.0,top: 10.0),
                width: 50.0,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0x47852369),
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage(tweetNew.imgBig),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(margin: EdgeInsets.only(top: 3.0),child: Text(tweetNew.author)),
                    Container(margin: EdgeInsets.only(top: 10.0),child: Text(tweetNew.pubDate)),
                  ],
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0,top: 10.0),
            alignment: Alignment.centerLeft,
            child: Text(tweetNew.body,softWrap: true,overflow: TextOverflow.ellipsis,maxLines: 2,),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10.0),
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 10.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.arrow_forward),
                      Text('转发'),
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.message),
                    Text('评论'),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 10.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.grade),
                      Text('点赞'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
