import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
class NewsListItem extends StatelessWidget {
  final Map<String,dynamic> newsList;
  NewsListItem({this.newsList}):assert(newsList!=null);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Toast.show("暂未处理点击", context,duration:Toast.LENGTH_SHORT,gravity: Toast.BOTTOM);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Color(0x47852369),
              width: 1.0,
            ),
          ),
        ),
        child: Column(
          children: <Widget>[
            Text('@${newsList['author']} ${newsList['title']}  ${newsList['pubDate']}'),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Icon(Icons.message),
                Text('${newsList['commentCount']}'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
