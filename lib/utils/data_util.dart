import 'package:openchina/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataUtil {
  static const String SP_ACCESS_TOKEN = 'access_token';
  static const String SP_REFRESH_TOKEN = 'refresh_token';
  static const String SP_UID = 'uid';
  static const String SP_TOKEN_TYPE = 'token_type';
  static const String SP_EXPIRES_IN = 'expires_in';
  static const String SP_IS_LOGIN = 'is_login';

  static const String SP_USER_GENDER = 'gender';
  static const String SP_USER_NAME = 'name';
  static const String SP_USER_LOCATION = 'location';
  static const String SP_USER_ID = 'id';
  static const String SP_USER_AVATAR = 'avatar';
  static const String SP_USER_EMAIL = 'email';
  static const String SP_USER_URL = 'url';

  //{"access_token":"8644a7e1-a097-496d-9891-e29a9419c983","refresh_token":"0ffc807d-ffc8-4345-90f0-1156726575b4","uid":1991319,"token_type":"bearer","expires_in":604799}
  //保存登录信息
  static Future<void> saveLoginInfo(Map<String, dynamic> map) async {
    if (map != null && map.isNotEmpty) {
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp
        ..setString(SP_ACCESS_TOKEN, map[SP_ACCESS_TOKEN])
        ..setString(SP_REFRESH_TOKEN, map[SP_REFRESH_TOKEN])
        ..setInt(SP_UID, map[SP_UID])
        ..setString(SP_TOKEN_TYPE, map[SP_TOKEN_TYPE])
        ..setInt(SP_EXPIRES_IN, map[SP_EXPIRES_IN])
        ..setBool(SP_IS_LOGIN, true);
    }
  }

  //清除数据
  static Future<void> clearLoginInfo() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp
      ..setString(SP_ACCESS_TOKEN, '')
      ..setString(SP_REFRESH_TOKEN, '')
      ..setInt(SP_UID, -1)
      ..setString(SP_TOKEN_TYPE, '')
      ..setInt(SP_EXPIRES_IN, -1)
      ..setBool(SP_IS_LOGIN, false);//清空token
    sp.clear();//全部清空
  }

  //判断是否登录
  static Future<bool> isLogin() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    bool isLogin = sp.getBool(SP_IS_LOGIN);
    return isLogin != null && isLogin;
  }

  static Future<String> getTokenAccess() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String accecc_token = sp.getString(SP_ACCESS_TOKEN);
    return accecc_token;
  }

  static savaUserInfo(Map<String, dynamic> map) async {
    if (map != null && map.isNotEmpty) {
      //{"gender":"male","name":"有点凉了","location":"北京 海淀","id":1991319,"avatar":"https://www.oschina.net/img/portrait.gif","email":"120476536@qq.com","url":"https://my.oschina.net/u/1991319"}
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp
        ..setString(SP_USER_GENDER, map[SP_USER_GENDER])
      ..setString(SP_USER_NAME, map[SP_USER_NAME])
      ..setString(SP_USER_LOCATION, map[SP_USER_LOCATION])
      ..setInt(SP_USER_ID, map[SP_USER_ID])
      ..setString(SP_USER_AVATAR, map[SP_USER_AVATAR])
      ..setString(SP_USER_EMAIL, map[SP_USER_EMAIL])
      ..setString(SP_USER_URL, map[SP_USER_URL]);
      User user = new User(id: map[SP_USER_ID],
          name: map[SP_USER_NAME],
          gender: map[SP_USER_GENDER],
          avatar: map[SP_USER_AVATAR],
          email: map[SP_USER_EMAIL],
          location: map[SP_USER_LOCATION],
          url: map[SP_USER_URL]);
      return user;
    }else{
      return null;
    }
  }
  static Future<User> getUserInfo() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    bool isLogin = sp.getBool(SP_IS_LOGIN);
    if(isLogin == null && !isLogin){
      return null;
    }
    User user = new User();
    user.id = sp.getInt(SP_USER_ID);
    user.name = sp.getString(SP_USER_NAME);
    user.gender = sp.getString(SP_USER_GENDER);
    user.avatar = sp.getString(SP_USER_AVATAR);
    user.email = sp.getString(SP_USER_EMAIL);
    user.location = sp.getString(SP_USER_LOCATION);
    user.url = sp.getString(SP_USER_URL);
    return user;
  }
}
