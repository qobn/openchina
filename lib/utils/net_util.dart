import 'package:http/http.dart' as http;
class NetUtil{
  static Future<String> get(String url,Map<String,dynamic> params) async{
    if(url!=null && params!=null && params.isNotEmpty){
      StringBuffer sb = new StringBuffer('?');
      params.forEach((key,value){
        sb.write('$key=$value&');
      });
      String paramsStr = sb.toString().substring(0,sb.length-1);
      url+=paramsStr;
    }
    print('NetUtils:$url');
    http.Response response = await http.get(url);
    return response.body;
  }
  static Future<String> post(String url,Map<String,dynamic> params) async{
    http.Response response = await http.post(url,body: params);
    return response.body;
  }
}