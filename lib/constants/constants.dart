abstract class AppColors{
//  应用主题色
  static const APP_THEME=0xff63ca6c;
  static const APPBAR=0xffffffff;
  static const CONTAINER_BACK=0xffffffff;
  static const DIVIDER_LINE=0x66666666;
}
abstract class AppInfo{
  static const String CLIENT_ID='K1TI5ZxqRUgx50vlAHR7';
  static const String CLIENT_SECRET='cOuOvT1uKD24x0OdUEOmksHHukM6kyDq';
  static const String REDIRECT_URI='https://www.baidu.com/';
  static const String AUTHORIZATION_CODE='authorization_code';
  static const String JSON='json';
}
abstract class AppUrls{
  static const String HOST = 'https://www.oschina.net';
  //授权登录
  static const String OAUTH2_AUTHORIZE = HOST+'/action/oauth2/authorize';
  static const String OAUTH2_TOKEN = HOST+'/action/openapi/token';
  static const String OPENAPI_USER = HOST+'/action/openapi/user';
  static const String USER_INFORMATION = HOST+'/action/openapi/user_information';
  static const String MY_INFORMATION = HOST+'/action/openapi/my_information';
  static const String MESSAGE_LIST = HOST+'/action/openapi/message_list';
  static const String NEWS_LIST = HOST+'/action/openapi/news_list';
  static const String NEWS_DETAIL = HOST+'/action/openapi/news_detail';
  static const String TWEET_LIST = HOST+'/action/openapi/tweet_list';
}